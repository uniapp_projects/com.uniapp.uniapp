var server_url="http://cp-cms.actechsupport.online/"; 

var server_route="api/v1/"; 

var product_key="eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzI1NiJ9.eyJhcHBfaWQiOjEsInVzZXJfaWQiOiIyIiwiY3JlYXRlZCI6MTUzMTI1NTM4MCwicGFja2FnZV90eXBlIjoxfQ.6uHfSSS7-C2or3LpXQApLklkLy26PPKb7kS7Zj3mzPs"; 

var settings={
  "general": {
    "product_key": "eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzI1NiJ9.eyJhcHBfaWQiOjEsInVzZXJfaWQiOiIyIiwiY3JlYXRlZCI6MTUzMTI1NTM4MCwicGFja2FnZV90eXBlIjoxfQ.6uHfSSS7-C2or3LpXQApLklkLy26PPKb7kS7Zj3mzPs",
    "app_title": "Uni app",
    "menu_icon": "fa fa-bug",
    "menu_slug": "404",
    "menu_display": "yes",
    "menu_position": "footer",
    "menu_header_show": "yes",
    "header_type": "default",
    "header_title_display": "yes",
    "header_title_position": "center",
    "disable_swipe": "true",
    "active_menu": "1",
    "status_bar_display": "true",
    "status_bar_background_color": "#ff0000",
    "content_sync_type": "background",
    "side_panels_display": "yes",
    "app_require_auth": "no"
  },
  "side_panel": {
    "show_left_menu": "yes",
    "left_menu_type": "default",
    "show_right_menu": "yes",
    "right_menu_type": "default"
  },
  "product": {
    "show_cart": "no",
    "cart_position": "right",
    "currency": "INR",
    "default_product_cod_available": "no",
    "default_product_cod_text": "Cash On Delivery",
    "default_product_cod_subtext": "Cash On Delivery",
    "default_product_cod_time": "10"
  },
  "auth": {
    "login_api_path": "auth/login",
    "signup_api_path": "auth/signup",
    "login_type": "base_username"
  },
  "logo": {
    "logo_default": "http://cp-cms.actechsupport.online/wp-content/uploads/2018/07/payulogo.png"
  },
  "color": {
    "header_bg_color_transparent": "yes",
    "header_bg_color": "#000000",
    "header_text_color": "#ffffff",
    "header_icon_color": "#ffffff",
    "left_side_panel_icon_color": "#ffffff",
    "left_side_panel_inner_icon_color": "#ffffff",
    "left_side_panel_inner_header_bg_color": "#ffffff",
    "left_side_panel_inner_body_bg_color": "#ffffff",
    "right_side_panel_icon_color": "#ffffff",
    "right_side_panel_inner_icon_color": "#ffffff",
    "right_side_panel_inner_header_bg_color": "#ffffff",
    "right_side_panel_inner_body_bg_color": "#ffffff",
    "main_menu_background_color": "#000000",
    "main_menu_bg_color_transparent": "no",
    "main_menu_icon_color": "#ffffff",
    "main_menu_tac_indicator_color": "#000000",
    "body_background_color": "#ffffff"
  }
}; 

